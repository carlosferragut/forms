import GenericForm, { FormProps } from '../components/GenericForm'
import Loading from '../components/loading'

export default function Home() {
  const renderForm = ({register, errors, isSubmitting}: FormProps) => {
    return <>
        <label htmlFor="email">Email</label>
        <input type="email" autoComplete="email"
               {...register("email", {required: true})} />
        <div className="error">{errors.email?.message}</div>
        
        <label htmlFor="date">Date</label>
        <input type="date" {...register("date", {required: true})} />
        <div className="error">{errors.date?.message}</div>
        
        <button disabled={isSubmitting}>
            {isSubmitting ? <Loading/> : "Submit"}
        </button>
    </>;       
  }

  const renderForm2 = ({register, errors, isSubmitting}: FormProps) => {
    return <>
        <label htmlFor="email">Email</label>
        <input type="email" autoComplete="email"
               {...register("email", {required: true})} />
        <div className="error">{errors.email?.message}</div>
        <div className="error">{errors.date?.message}</div>
        
        <button disabled={isSubmitting}>
            {isSubmitting ? <Loading/> : "Submit"}
        </button>
    </>;       
  }

  return (
    <>
      <h2>First form</h2>
    <GenericForm url="/api/form" renderForm={renderForm} />
      <h2>Second form</h2>
    <GenericForm url="/api/form" renderForm={renderForm2} />
    </>
  )
}
